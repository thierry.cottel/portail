# Trouver ses voisins dans le jeu de la vie

def hauteur(g):
    """Renvoie la hauteur (le nombre de lignes) de la grille g.
    >>> hauteur([[0,1,1],[1,0,0]]) == 2
    True
    >>> hauteur([[0,1,1,1],[0,0,1,0],[1,1,0,0]]) == 3
    True
    """
    return len(g)


def largeur(g):
    """Renvoie la largeur (le nombre de colonnes) de la grille g.
    La largeur de la grille vide est 0.
    >>> largeur([[0,1,1],[1,0,0]]) == 3
    True
    >>> largeur([[0,1,1,1],[0,0,1,0],[1,1,0,0]]) == 4
    True
    >>> largeur([[]]) == 0
    True
    >>> largeur([]) == 0
    True
    """
    if len(g) > 0 :
        return len(g[0])
    else:
        return 0

def voisins_case(g, x, y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    voisins=[]                                             
    vx = x-1                                                   
    vy = y-1                                                 
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                                     
    vx = x-1                                                        
    vy = y                                                          
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                                     
    vx = x-1                                                        
    vy = y+1                                                        
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x                                                     
    vy = y-1                                                   
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x                                                   
    vy = y+1                                                 
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x+1                                                   
    vy = y-1                                                   
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x+1                                                   
    vy = y                                                     
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x+1                                                   
    vy = y+1                                                   
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    return voisins

def est_dans_grille(g,x,y):
    """Vérifie que les coordonnées (x,y) correspondent à une case de la grille g.
    >>> est_dans_grille([[0,0,1],[1,1,0]],0,1)
    True
    >>> est_dans_grille([[0,0,1],[1,1,0]],4,1)
    False
    >>> est_dans_grille([[0,0,1],[1,1,0]],1,4)
    False
    >>> est_dans_grille([[0,0,1],[1,1,0]],4,4)
    False
    """
    return 0 <= x and x < hauteur(g) and 0 <= y and y < largeur(g)

def voisins_case_2(g, x, y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case_2([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case_2([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case_2([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    voisins=[]
    vx = x-1
    vy = y-1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x-1
    vy = y
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x-1
    vy = y+1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x
    vy = y-1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x
    vy = y+1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x+1
    vy = y-1
    if est_dans_grille(g,vx,vy): 
        voisins = voisins + [g[vx][vy]]
    vx = x+1
    vy = y
    if est_dans_grille(g,vx,vy): 
        voisins = voisins + [g[vx][vy]]
    vx = x+1
    vy = y+1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    return voisins

def voisins_case_3(g,x,y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case_3([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case_3([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case_3([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    voisins = []                                 
    mv = [-1,0,1]                                        
    for dx in mv:                                             
        for dy in mv:                                         
            if (dx != 0 or dy !=0) and est_dans_grille(g,x+dx,y+dy): 
                voisins = voisins + [g[x+dx][y+dy]]   
    return voisins

def voisins_case_4(g, x, y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case_4([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case_4([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case_4([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    mv = [-1,0,1]                                        
    directions = [(dx,dy) for dx in mv for dy in mv if (dx,dy) != (0,0)] 
    return [g[x+dx][y+dy] for (dx,dy) in directions if est_dans_grille(g,x+dx,y+dy)]

import doctest
doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
