# A propos d'écriture de code

**"Ca marche" n'est toujours pas suffisant..."  


Nous étudions ici quelques exemples commentés de code trouvés dans les TP wator de la promo précédente. 
C'est l'occasion d'évoquer des choses "à ne pas faire" ou de montrer "comment faire autrement".
Il s'agit aussi de parler de "bonnes pratiques" et d'évaluation de code.

La granularité et l'importance des remarques sont très variables. 

## Choisir correctement les identificateurs
 
###  pour les fonctions
 
 ```python
 def afficher_grille(population):
    """
    Affiche la grille:
        ' ': case vide
        'R': requin
        'T': thon

    :param population: (dict) dictionnaire
    :return: liste de liste
    """
    grille = creation_grille()

    for p in population:
        (x, y) = p
        car = 'T'
        if 'energie' in population[p].keys():
            car = 'R'
        grille[y][x] = car

    return grille
```

`afficher_grille` ne fait pas d'affichage mais renvoie une liste, éventuellement à renommer en `population_to_char`


### ou les paramètres 

```python
def deplace_vers_mer(e,case,case_mer,grille,gestation,energie=0):
    """
    Retourne la grille après le déplacement d'une espèce vers une case de mer
    :param e: (int) représente le chiffre associé au poisson (1 pour un thon, 2 pour un requin)
    :param case: (tuple) coordonnées de la case étudiée sous la forme (x,y)
    :param case_mer: (tuple) coordonnées de la case de mer vers laquelle le poisson se déplace
    :param grille: (list) représente la grille du jeu
    :param gestation: (int) durée de gestation de l'espèce ayant déjà été mise à jour
    :param energie: (int) energie de l'espèce ayant déjà été mise à jour ( 0 si non concerné)
    :return: (list) une grille du jeu mise à jour après le délacement du poisson
    :CU:  l'énergie et la gestation évolue avant le déplacement, les naissances sont gérées par la fonction
    
    >>> deplace_vers_mer(1,(0,0),(0,1),[[(1,2,0),(1,2,0),(0,0,0)],[(0,0,0),(2,5,3),(0,0,0)]],1)
    [[(0, 0, 0), (1, 2, 0), (0, 0, 0)], [(1, 1, 0), (2, 5, 3), (0, 0, 0)]]
    
    >>> deplace_vers_mer(2,(1,1),(2,1),[[(1,2,0),(0,0,0),(0,0,0)],[(0,0,0),(2,5,3),(0,0,0)]],4,2)
    [[(1, 2, 0), (0, 0, 0), (0, 0, 0)], [(0, 0, 0), (0, 0, 0), (2, 4, 2)]]
    
    """
        
   (...)
```   
les paramètres `case` et `case_mer` ne sont pas des cases mais des coordonnées.

Proposition : 

```python
def deplace_vers_mer(e,coord_depart,coord_destination,grille,gestation,energie=0):       
        
   (...)
```   

Idem pour le choix des identificateurs de variables.

## Structure conditionnelle et résultat valeur booléen

```python
def verifier_case_vide(grille,x,y):
    """
    prédicat pour teste sur une case de grille est vide de poissons
    c'est à dire contient 0
    :param: grille(list); x,y (int)
    :return: (bool)
    >>> verifier_case_vide(grille,2,1)
    True
    """
    if grille[x][y]==0:
        return True 
    else: 
        return False
```

s'écrit en fait

```python
def verifier_case_vide(grille,x,y):
    """
	(...)
    """
    return grille[x][y]==0
```

Il en est de même pour le schéma
```python
if condition:
   return False
else:
   return True
```

qui s'écrit bien sûr

```python
return not condition 
```


## Listes en compréhension
 
```python
def creer_grille_vierge(nbLignes,nbColonnes):
    '''
    envoie une liste de listes
    correspondant à une grille du jeu de la vie aux dimensions souhaitées, ne
    contenant aucune cellule
    : param  nbLignes : (int) entier indiquant le nombre de lignes
    : param  nbColonnes : (int) entier indiquant le nombre de colonnes
    :Exemple : >>> creer_grille_vierge(3,2)
    [[[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    '''
    liste = []
    for l in range(0,nbLignes): # crée les lignes
        liste.append([])
        for _ in range(0,nbColonnes): # crée les colonnes
            liste[l].append([0]*3) # crée [0,0,0]
    return liste

```

NB : dans docstring, `:Exemple : >>> creer_grille_vierge(3,2)` n'est pas un *doctest*, il faudrait passer à la  ligne avant `>>>`


devient
```python
def creer_grille_vierge_bis(nbLignes,nbColonnes):
    '''
    ...
    :Exemple :
    >>> creer_grille_vierge(3,2)
    [[[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    '''
    return [ [ [0,0,0]  for ligne in range(nbColonnes)] for _ in range(nbLignes) ]
``` 

*NB* ici illustration variable anonyme `_`, on aurait pu/dû faire pareil pour variable `ligne`.


### Autre exemple

 
```python
def creerMer(nL, nC):
    gr = []
    for i in range(nL):
        uneLigne = [0]*nC
        gr.append(uneLigne)
    return gr
```
devient 
```python
def creerMer_bis(nL,nC):
    #return [ [0]*nC for _ in range(nL) ]
    return [ [0 for _ in range(nC)] for _ in range(nL)]
```

### Autre autre exemple

```python
def pos_possibles(grille):
    """
    renvoie une liste des positions (i,j) associés au cases de la grille
    :param grille:(list)
    :return: (int)
    >>> pos_possibles(grille_vide(3, 2))
    [(0,0),(0,1),(1,0),(1,1),(2,0),(2,1)]
    """
    retour=[]
    for i in range(largeur(grille)):
        for j in range(hauteur(grille)):
            retour.append((i,j))
    return retour
```

devient

```python
def pos_possibles(grille):
    """
    (...)
    """
    return [ (i,j) for i in range(largeur(grille)) for j in range(hauteur(grille)) ]
```
produit
```python
[(0,0), (0,1), (0,2), ..., (1,0), ... ]
```

### Autre exemple avec une condition 
```python
def nombrePoissons(gr, typeP):
    """
    Compte le nombre de poissons pour le type demandé.
    :param gr: (liste de listes) une grille de la mer
    :param typeP: (string) THON ou REQUIN
    :return: (int) le nombre de poisson du typeP

    """
    nbP = 0
    for l in gr:
        for c in l:
            if c != 0:
                if c['type'] == typeP:
                    nbP += 1
    return nbP
```


devient

```python
def nombrePoissons_bis(gr, typeP):
    """
	...
    """
    return len ( [ 1 for l in gr for c in l if c != 0 and c['type'] == typeP ]  )
	
```

ou avec des identificateurs plus explicites :
```python
def nombrePoissons_ter(grille, typePoisson):
    """
	...
    """
    return len ( [ 1 for ligne in grille for case in ligne if case != 0 and c['type'] == typePoisson ]  )
	
```



### Danger : partage de valeurs

```python
def cree_grille(largeur, hauteur):
	'''
	'''
	ligne = [ 0 for _ in range(largeur) ]
	grille = [ligne for _ in range(hauteur) ]
	return grille
```

([avec PythonTutor](http://pythontutor.com/live.html#code=largeur%20%3D%203%0Ahauteur%20%3D%204%0Aligne%20%3D%20%5B%200%20for%20_%20in%20range%28largeur%29%20%5D%0Agrille%20%3D%20%5Bligne%20for%20_%20in%20range%28hauteur%29%20%5D%0A&cumulative=true&curInstr=0&heapPrimitives=false&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)) 

Situation dangereuse évidemment non spécifiquement due à la construction par compréhension :
```python
def cree_grille_bis(largeur, hauteur):
    '''
    '''
    # création d'une ligne
    for _ in range(hauteur):
        ligne = ligne.append(0)
    # création de grille
    for _ in range(largeur):
        grille.append(ligne)
    return grille
```

### Dans le même genre 
(très proche de `creerMer` ci-dessus)

```python
def creer_grille(n,p):
    """
    Renvoie une tableau à n lignes et p colonnes
    :param: n,p (int)
    :return: L (list)
    >>> creer_grille(3,2)
    [[0, 0], [0, 0], [0, 0]]
    """
    L=[]
    for _ in range(n):
        L.append([0]*p)
    return L
    # return [0 for _ in range(n) for _ in range(p)]
```
transformons, à peine, ce code en :

```python
def creer_grille(n,p):
    valeur = 0
    L=[]
    for _ in range(n):
        L.append([valeur]*p)
    return L
    # return [valeur for _ in range(n) for _ in range(p)]
```

On a un schéma potentiellement dangereux : si `valeur` est mutable (ce qui se produira avec une "valeur objet").


```python
def creer_grille(n,p):
    valeur = [0]
    L=[]
    for _ in range(n):
        L.append([valeur]*p)
    return L
    # return [valeur for _ in range(n) for _ in range(p)]
```

```python
>>> g = cree_grille(3,2)
[[ [0] , [0] ], [ [0] , [0] ], [ [0] , [0] ]]
>>> g[1][1][0] = 8
>>> g
[[[8], [8]], [[8], [8]], [[8], [8]]]
```




## Destructuration d'un tuple à l'affectation 

`liste_voisins` est une liste de coordonnées

```python
    choix=random.randrange(n)
    case_voisine_testee=liste_voisins[choix]#case testée dans la liste
    if grille[case_voisine_testee[1]][case_voisine_testee[0]][0]==e:# si la case voisine contient un thon
```
devient (au passage présentation de `random.choice`) :
```python
    x,y=random.choice(liste_voisins)   #case testée dans la liste
    if grille[y][x][0]==e:# si la case voisine contient un thon			
```

### Autre exemple

```python
def deplace_vers_mer(e,case,case_mer,grille,gestation,energie=0):
    """
	(...)
    """
        
    if gestation==0:# si la durée de gestation devient nulle
        if e==1 :
            grille[case_mer[1]][case_mer[0]]=init_case(e)#le poisson se déplace et gestation remise à valeur initiale
        elif e==2:
            grille[case_mer[1]][case_mer[0]]=(2,G_REQUIN,energie)
        grille[case[1]][case[0]]=init_case(e)# naissance d'un poisson sur la case quittée
    else :
        grille[case_mer[1]][case_mer[0]]=(e,gestation,energie)# le poisson se déplaçant perd 1 de gestation
        grille[case[1]][case[0]]=init_case(0)# case quitée devient une mer
    return grille
```
devient
```python
def deplace_vers_mer(e,case,case_mer,grille,gestation,energie=0):
    """
	(...)
    """
    x, y = case
    x_dest, y_dest = case_mer
    if gestation==0:# si la durée de gestation devient nulle
        if e==1 :
            grille[y_dest][x_dest]=init_case(e)#le poisson se déplace et gestation remise à valeur initiale
        elif e==2:
            grille[y_dest][x_dest]=(2,G_REQUIN,energie)
        grille[y][x]=init_case(e)# naissance d'un poisson sur la case quittée
    else :
        grille[y_dest][x_dest]=(e,gestation,energie)# le poisson se déplaçant perd 1 de gestation
        grille[y][x]=init_case(0)# case quitée devient une mer
    return grille
```

Gain en lisibilité.


## Nommer les éléments
**Bonne pratique** : Il faut nommer les constantes (valeurs numériques, chaînes de catactères, etc.)
```python
def generation_suivante(l):
    '''
    (...)
    '''
    # tirage d'une case aléatoirement dans la mer
    lig,col = tirage_case(largeur_grille(l), hauteur_grille(l))
    # si la case est un thon
    if l[lig][col][0] == 1:
        return comportement_thon(l,lig,col)
    elif l[lig][col][0] == 2:
        return comportement_requin(l,lig,col)
    else :
        return l
```

Absence de sens des valeurs `1`et `2`. Nécessite un commentaire (non présent pour requin).
Il faut nommer les valeurs. Par exemple :

```python
MER = 0
THON = 1
REQUIN = 2

def generation_suivante(l):
    '''
    (...)
    '''
    # tirage d'une case aléatoirement dans la mer
    lig,col = tirage_case(largeur_grille(l), hauteur_grille(l))
    if l[lig][col][0] == THON:
        return comportement_thon(l,lig,col)
    elif l[lig][col][0] == REQUIN:
        return comportement_requin(l,lig,col)
    else :
        return l
```
On constate qu'on a gagné en lisibilité, le commentaire avant le `if` n'est plus nécessaire 

Mais, mieux encore car plus explicite, on peut définir des fonctions :

```python
MER = 0
THON = 1
REQUIN = 2

def est_un_thon(case):
    return case[0] == THON
def est_un_requin(case):
    return case[0] == REQUIN
	
def generation_suivante(l):
    '''
    (...)
    '''
    # tirage d'une case aléatoirement dans la mer
    lig,col = tirage_case(largeur_grille(l), hauteur_grille(l))
    case = l[lig][col]
    if est_un_thon(case):
        return comportement_thon(l,lig,col)
    elif est_un_requin(case):
        return comportement_requin(l,lig,col)
    else :
        return l
```

De plus, le code de `generation_suivante` devient beaucoup plus indépendant de
la façon dont on représente les données.

Si au lieu d'une liste la valeur d'une case est un dictionnaire, il suffit d'adapter les codes de `est_un_thon` et `est_un_requin` sans avoir à omdifier `generation_suivante` :

```python
MER = 0
THON = 1
REQUIN = 2

def est_un_thon(case):
    return case['espece'] == THON
def est_un_requin(case):
    return case['espece'] == REQUIN
	
def generation_suivante(l):
    '''
    (...)
    '''
    ... /!\ INCHANGE  /!\
```



## Nommer les éléments (2) 

Il faut nommer les fonctionnalités pour ajouter de la sémantique au code écrit. On facilite le travail du relecteur de code mais aussi l'écriture de code qui est plus proche du discours en langue naturelle.

Par exemple, on trouve dans un même code de très nombreuses occurrences de `grille[y][x][0] =='T'`, ` case[0] =='T'` ou `poisson[0] =='T'`
et idem avec les requins

Il faut définir des fonctions
```python
def est_un_thon(case):
    return case[0] == 'T'
def est_un_requin(case):
    return case[0] == 'R'
```
on  utilise maintenant dans le code `est_un_thon(grille[y][x])`, `est_un_thon(case)`, `est_un_thon(poisson)` : **gain en lisibilité**

(et il faudrait définir en plus des constantes pour remplacer `'T'` et `'R'` dans le code)



 


## Il faut décomposer

Un grand nombre de lignes de code, ou un grand nombre de structures itératives ou conditionnelles imbriquées, dans une fonction est un mauvais symptôme.

```python
def comportement_requin(l,lig,col):
    # ---------DEPLACEMENT ----------
    l[lig][col][2] = l[lig][col][2] - 1
    #if l_dest == 0 and c_dest == 0: # s'il n'existe aucune case voisine avec un thon
    if len(voisins_case(l,col,lig,1)) != 0: # si on peut se déplacer sur une case avec un thon
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,1))
        tps_gest = l[lig][col][1]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,ENERGIE]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]
    elif len(voisins_case(l,col,lig,0)) != 0: # sinon si on peut se déplacer sur une case vide
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,0)) # on choisit une case vide
        tps_gest = l[lig][col][1]
        energie = l[lig][col][2]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,energie]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]     
    else : # pas de déplacement il faut diminuer la gestation
        l_dest,c_dest = 0,0
        tps_gest = l[lig][col][1]
        energie = l[lig][col][2]
        l[lig][col] = [2,tps_gest-1,energie]
    # ---------ENERGIE ----------    
    if l[lig + l_dest][col + c_dest][2] == 0: # si le niveau d'énergie tombe à 0
        l[lig + l_dest][col + c_dest] = [0,0,0] # le requin meurt
    else:
        # ---------REPRODUCTION ---------- 
        if l[lig + l_dest][col + c_dest][1] == 0:
            # reproduction seulement si il y a eu déplacement
            if(l_dest,c_dest) != (0,0): 
                l[lig][col] = [2,TPS_GEST_REQUIN,ENERGIE]
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN
            else: # réinitialisatiojn gestation
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN
    return l
```

On peut remarquer que la structure de décomposition apparaît nettement et a été (bien) faite : 

```python
def comportement_requin(l,lig,col):
    # ---------DEPLACEMENT ----------
	...
    # ---------ENERGIE ----------    
		...
        # ---------REPRODUCTION ---------- 
        ...
    return l
```

Il faut donc l'utiliser, ce qui donnerait : 

```python
def comportement_requin_refactor(l,lig,col):    
    requin = l[lig][col]
    decrement_energie(requin)
    # fournit le déplacement du requin après déplacement (ou pas si 0,0 )
    l_dest,c_dest = deplacement_requin_bis(l,lig,col)
    if requin_est_mort(l[lig + l_dest][col + c_dest]): 
        l[lig + l_dest][col + c_dest] = [0,0,0] # le requin meurt
    else:
        reproduction_requin(l,lig,col,l_dest,c_dest)
    return l

def decremente_energie(requin):
    requin[2] = requin[2] - 1
 
def requin_est_mort(requin):
    return requin[2] == 0
	
def deplacement_requin(l,lig,col):
    requin = l[lig][col]
    #if l_dest == 0 and c_dest == 0: # s'il n'existe aucune case voisine avec un thon
    if len(voisins_case(l,col,lig,1)) != 0: # si on peut se déplacer sur une case avec un thon
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,1))
        tps_gest = requin[1]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,ENERGIE]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]
    elif len(voisins_case(l,col,lig,0)) != 0: # sinon si on peut se déplacer sur une case vide
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,0)) # on choisit une case vide
        tps_gest = requin[1]
        energie = requin[2]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,energie]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]     
    else : # pas de déplacement il faut diminuer la gestation
        l_dest,c_dest = 0,0
        tps_gest = requin[1]
        energie = requin[2]
        l[lig][col] = [2,tps_gest-1,energie]
    return l_dest,c_dest

def reproduction_requin(l,lig,col,l_dest,c_dest):
    if l[lig + l_dest][col + c_dest][1] == 0:
            # reproduction seulement si il y a eu déplacement
            if(l_dest,c_dest) != (0,0): 
                l[lig][col] = [2,TPS_GEST_REQUIN,ENERGIE]
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN
            else: # réinitialisatiojn gestation
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN


		
```

A nouveau, plus besoin des commentaires dans `comportement_requin_refactor`. Plus de petits codes. **Diviser pour régner.**


Mais, pour aller plus loin, on peut remarquer qu'il y a des répétitions de calcul de `voisins_case(l,col,lig,?)`
et, pour être cohérent, la diminution de la gestation devrait être gérée comme l'énergie avec une fonction en dehors de `deplacement_requin` 
appelée dans `comportement_requin_refactor`(principe de responsabilité unique par rapoort au nom de la fonction)


 on peut donc retravailler encore un peu plus : 

```python
def comportement_requin_refactor_bis(l,lig,col):    
    requin = l[lig][col]
    decrement_energie(requin)
    decrement_gestation(requin)
    # fournit le déplacement du requin après déplacement (ou pas si 0,0 )
    l_dest,c_dest = deplacement_requin_ter(l,lig,col)
    if requin_est_mort(l[lig + l_dest][col + c_dest]): 
        l[lig + l_dest][col + c_dest] = [0,0,0] # le requin meurt
    else:
        reproduction_requin(l,lig,col,l_dest,c_dest)
    return l
    
def deplacement_requin_ter(l,lig,col):
    requin = l[lig][col]
    tps_gest = requin[1]
    energie = requin[2]
    # si pas de déplacement 
    l_dest,c_dest = 0,0
    #l_dest == 0 and c_dest == 0 s'il n'existe aucune case voisine avec un thon
    voisines_avec_thons = voisins_case(l,col,lig,1)
    if len(voisines_avec_thons) != 0: # si on peut se déplacer sur une case avec un thon
        l_dest,c_dest = calcul_deplacement(voisines_avec_thons)
        energie = ENERGIE
    else:
        voisines_vides = voisins_case(l,col,lig,0) 
        if len(voisines_vides) != 0: # sinon si on peut se déplacer sur une case vide
            l_dest,c_dest = calcul_deplacement(voisines_vides) # on choisit une case vide
    if (l_dest,c_dest) != (0,0):
        # s'il y a eu déplacement suppression dans la case d'origine
        l[lig][col] = [0,0,0]             
	# et ajout dans nouvelle case
	l[lig + l_dest][col + c_dest] = [2,tps_gest,energie]
    return l_dest,c_dest

def decremente_gestation,(poisson):
    poisson[1] = poisson[1] - 1
```

Comparer la lisibilité de cette troisième version avec la première. On est passer de 35 à 20 lignes.



On peut cependant noter que l'utilisation des listes n'aide pas à identifier énergie et gestation et nécessite donc la définition de variable pour la lisibilité : 
`tps_gest = requin[1]` et `energie = requin[2]`


## Un autre exemple

```python

def thon_requin(grille,ges,gestation_init_thon,gestation_init_req,energie,energie_init):
    
    cpt1=0
    cpt2=0
    
    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):

            x=randint(max(0,i-1),min(largeur_grille(grille)-1,i+1))
            y=randint(max(0,j-1),min(hauteur_grille(grille)-1,j+1))
            
            # case vide
            if grille[i][j]==0:   
                x=randint(max(0,i-1),min(largeur_grille(grille)-1,i+1))
                y=randint(max(0,j-1),min(hauteur_grille(grille)-1,j+1))
            
            # case thon
            elif grille[i][j]==2:
                cpt2+=1
                
                if (x,y)==(i,j) or grille[x][y]==1:
                    if ges[i][j]==0:
                        ges[i][j]=gestation_init_thon
                    else:
                        ges[i][j]-=1
                else:
                    if grille[x][y]==0:
                        grille[x][y]=2
                        
                        if ges[i][j]==0:
                            grille[i][j]=2
                            ges[i][j]=gestation_init_thon
                        else:
                            grille[i][j]=0
                            ges[i][j]-=1
            
            # case requin
            elif grille[i][j]==1:
                cpt1+=1
                energie[i][j]-=1
                
                if energie[i][j]!=0:
                    
                    if nb_cellules_voisins(grille,i,j,2)==0:
                                    
                        if (x,y)==(i,j) or grille[x][y]==1:
                            if ges[i][j]==0:
                                ges[i][j]=gestation_init_req
                                
                            else:
                                ges[i][j]-=1
                        
                        elif (x,y)!=(i,j) and grille[x][y]==0:
                            grille[x][y]=1
                            
                            if ges[i][j]==0:
                                grille[i][j]=1
                                ges[x][y]=gestation_init_req
                                ges[i][j]=gestation_init_req
                                energie[x][y]=energie[i][j]
                                energie[i][j]=energie_init
                                
                            else:
                                grille[i][j]=0
                                ges[x][y]=ges[i][j]-1
                                ges[i][j]=0
                                energie[x][y]=energie[i][j]
                                energie[i][j]=0
                    
                    else:
                        if grille[x][y]!=2:
                            x=randint(max(0,i-1),min(largeur_grille(grille)-1,i+1))
                            y=randint(max(0,j-1),min(hauteur_grille(grille)-1,j+1))
                        
                        if (x,y)!=(i,j) and grille[x][y]==2:
                            grille[x][y]=1
                            
                            if ges[i][j]==0:
                                grille[i][j]=1
                                ges[x][y]=gestation_init_req
                                ges[i][j]=gestation_init_req
                                energie[x][y]=energie_init
                                energie[i][j]=energie_init
                                
                            else:
                                grille[i][j]=0
                                ges[x][y]=ges[i][j]-1
                                ges[i][j]=0
                                energie[x][y]=energie_init
                                energie[i][j]=0
                    
                else:
                    grille[i][j]=0
                    ges[i][j]=0
                    
    return grille,ges,energie,cpt2,cpt1           
```

Il faut décomposer pour **garder la maitrise sur le code** !

A nouveau la décomposition apparaît, on voit bien aux commentaires qu'elle est présente dans l'esprit du programmeur.
On devrait donc la retrouver dans le code !
```python

def thon_requin(grille,ges,gestation_init_thon,gestation_init_req,energie,energie_init):
    
    cpt1=0
    cpt2=0
    
    for j in range(hauteur_grille(grille)):
        for i in range(largeur_grille(grille)):

            x,y = choisir_case(i,j)
            # case vide
            gestion_case_vide(i,j,  grille,ges,gestation_init_thon,gestation_init_req,energie,energie_init)
            # case thon
            elif grille[i][j]==2:
		gestion_case_thon(i,j,x,y, grille,ges,gestation_init_thon,gestation_init_req,energie,energie_init)
            # case requin
            elif grille[i][j]==1:
                gestion_case_requin(i,j,x,y, grille,ges,gestation_init_thon,gestation_init_req,energie,energie_init)
                    
    return grille,ges,energie,cpt2,cpt1

def choisir_case( ...):
    ...
def gestion_case_thon( ...):
    ...
def gestion_case_requin( ...):
    ...
def gestion_case_vide( ...):
    ...
```
et retravailler aussi les fonctions ainsi introduites


## Typage

Il faut a priori avoir un seul type possible pour le résultat d'une fonction
```python
def trouverCaseLibre(gr, cases_voisines):
    """
	(...)
    :return: (tuple) les coordonnées d'une case libre
             (int) -1 s'il n'y a pas de case libre
    """
    for case in cases_voisines:
        if typeOccupationCase(gr, case) == 0:
            return case
    return -1
```
On peut utiliser `None` pour exprimer l'absence de résultat, 
```python
def trouverCaseLibre_bis(gr, cases_voisines):
    """
	(...)
    :return: (tuple) les coordonnées d'une case libre, 
             None si aucune
    """
    for case in cases_voisines:
        if typeOccupationCase(gr, case) == 0:
            return case
    return None
```

Ou déclencher une exception (qu'il faudra gérer)

```python
def trouverCaseLibre_ter(gr, cases_voisines):
    """
	(...)
    :return: (tuple) les coordonnées d'une case libre, 
             None si aucune
    """
    for case in cases_voisines:
        if typeOccupationCase(gr, case) == 0:
            return case
    #raise ValueError('pas de case libre')
    raise ValueNotFoundError('pas de case libre')
	
class ValueNotFoundError(Exception):
    '''	Raised when a searched value is not find '''
    def __init__(self,message):
        self.message = message
```

De même


```python
def recherche_case(liste,grille,e):
    """
    (...)
    :CU: la liste des coordonnées des cases voisines doit êtr fournie par la fonction 
    :return:(tuple or bool) retourne False si aucune case du type recherché n'est trouvée

    >>> recherche_case([(2, -1), (1, 0), (0, 0), (2, 1)],[[(1,2,0),(0,0,0),(0,0,0)],[(0,0,0),(2,5,3),(0,0,0)]],2)
    False    
    >>> recherche_case([(1, -1), (0, 0), (2, 0), (1, 1)],[[(1,2,0),(0,0,0),(0,0,0)],[(0,0,0),(2,5,3),(0,0,0)]],1)
    (0, 0)
    """
    
    liste_voisins=list(liste)
    n=4
    while(n>0):
        choix=random.randrange(n) # choix d'un nombre entier dans l'intervalle [0,4[ correspondant à une des 4 cases voisines
        
        case_voisine_testee=liste_voisins[choix]#case testée dans la liste
        if grille[case_voisine_testee[1]][case_voisine_testee[0]][0]==e:# si la case voisine contient un thon
            
            return case_voisine_testee
         
        else :
            del liste_voisins[choix]
            n=n-1
    return False
```

devient


```python
def recherche_case(liste,grille,e):
    """
    (...) 
    :return:(tuple) retourne une case du type recherché n'est trouvée ou Flase sinon   
    :Exemples:    
    >>> recherche_case([(2, -1), (1, 0), (0, 0), (2, 1)],[[(1,2,0),(0,0,0),(0,0,0)],[(0,0,0),(2,5,3),(0,0,0)]],2)
    None    
    >>> recherche_case([(1, -1), (0, 0), (2, 0), (1, 1)],[[(1,2,0),(0,0,0),(0,0,0)],[(0,0,0),(2,5,3),(0,0,0)]],1)
    (0, 0)
    """
    
    liste_voisins=list(liste)
    n=4
    while(n>0):
        choix=random.randrange(n) # choix d'un nombre entier dans l'intervalle [0,4[ correspondant à une des 4 cases voisines
        
        case_voisine_testee=liste_voisins[choix]#case testée dans la liste
        if grille[case_voisine_testee[1]][case_voisine_testee[0]][0]==e:# si la case voisine contient un thon
            
            return case_voisine_testee
         
        else :
            del liste_voisins[choix]
            n=n-1
    return None
```



Il faut respecter aussi le typage des éléments d'un tableau/liste, par
exemple ne pas mélanger des chaînes de caractères ' ' pour une case
vide et des dictionnaires, tuples ou listes pour les poissons





