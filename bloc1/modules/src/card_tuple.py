# version d'implémentation des cartes avec un tuple (valeur, couleur)

import random as alea
import builtins
    
## tuple of possible values and colors in ascending order
VALUES = ("as", "2", "3", "4", "5", "6", "7", "8", "9", "10", "valet", "dame", "roi")
COLORS = ("trèfle", "coeur", "carreau", "pique")


def create(value, color):
    return (color,value)

def get_color(card):
    return card[0]

def get_value(card):
    return card[1]

def compare_value(card_1, card_2):
    return VALUES.index(get_value(card_1)) - VALUES.index(get_value(card_2))

def compare_color(card_1, card_2):
    return COLORS.index(get_color(card_1)) - COLORS.index(get_color(card_2))

def compare(card_1, card_2):
    cmp_values = compare_value(card_1,card_2)
    if cmp_values == 0:
        return compare_color(card_1,card_2)
    else:
        return cmp_values

def equals(card_1,card_2):
    return compare(card_1,card_2) == 0

def random():
    value = alea.randrange(len(VALUES))
    color = alea.randrange(len(COLORS))
    return create(VALUES[value], COLORS[color])

def str(card):
    return '{} de {}'.format(get_value(card).upper(), get_color(card))

def print(card, end='\n'):
    builtins.print(str(card), end=end)

#===========================================

def main():
    some_card = create('10','coeur')
    print(some_card)
    other_card = random()
    print(other_card)
    builtins.print('comparaison {} et {} = {}'.format(str(some_card), str(other_card),compare(some_card,other_card)))

if __name__ == '__main__':
    main()