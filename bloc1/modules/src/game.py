
import card
import random

def create_deck():
    '''
    create and return a deck with all cards
    '''
    return [card.create(val, col) for col in card.COLORS for val in card.VALUES]

def deal_cards():
    '''
    randomly dealt cards in two decks, tuple of 2 dealt decks is returned
    '''
    deck = create_deck()
    nb_cards = len(deck)
    random.shuffle(deck)
    return (deck[:(nb_cards//2)], deck[(nb_cards//2):])

def one_shot_war():
    '''
    play a very simplified one shot war card game
    '''
    (deck_1,deck_2) = deal_cards()
    for i in range(len(deck_1)):  # cards number assumed to be equals in both decks
        one_round(deck_1[i],deck_2[i])
        
def one_round(card_1,card_2):
    '''
    play one round of the simplified war card game, only prints the round issue
    '''
    cmp_cards = card.compare_value(card_1,card_2)
    print('players plaid [{}] vs [{}]'.format(card.str(card_1), card.str(card_2)), end=' => ')
    if cmp_cards < 0 :
        print('player 1 wins')        
    elif cmp_cards > 0 :
        print('player 2 wins')
    else:
        print('war ! card have same values : {}'.format(card.get_value(card_1)))


if __name__ == '__main__':
    one_shot_war()
