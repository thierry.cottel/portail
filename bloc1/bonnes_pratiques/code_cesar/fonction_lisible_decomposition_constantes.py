# + des identificateurs plus lisibles
# + une fonction paramétrée
# + séparation du calcul et de l'affichage : le calcul peut être réutilisé
# + quelques commentaire aident à la compréhension
# + de petites fonctions dont le code est facile à lire et donc à faire évoluer
# + la décomposition du code est plus explicite, les commentaires dans le code deviennent superflus
# + des constantes nommées, donc plus explicites et plus faciles à changer
# - pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
# - pas de test de validation du code


CODE_a = ord('a') # 97
CODE_z = ord('z')

DEFAULT_DECALAGE = 1

def est_lettre(car):
    return ord(car) >= CODE_a and ord(car) <= CODE_z

def code_lettre(lettre, decalage):
    return chr ( CODE_a + (ord(lettre) - CODE_a + decalage) % 26 )

def code_cesar_caractere(car, decalage = DEFAULT_DECALAGE):
    if est_lettre(car):
        return code_lettre(car, decalage)
    else:
        return car

def code_cesar_mot(mot, decalage = DEFAULT_DECALAGE):
    result = ''
    for lettre in mot :
        result = result + code_cesar_caractere(lettre, decalage)
    return result

LONGUEUR_MOT_MIN = 3
def code_mot(mot, decalage = DEFAULT_DECALAGE):
    if len(mot) < LONGUEUR_MOT_MIN:
        return mot
    else:
        return code_cesar_mot(mot, decalage)

def code_phrase(phrase, decalage = DEFAULT_DECALAGE):
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        liste_code.append(code_mot(mot,decalage))    
    return ' '.join(liste_code)

def affiche_code_phrase(phrase, decalage = DEFAULT_DECALAGE):    
    print( phrase + ' -> ' +   code_phrase(phrase, decalage) )
    

phrase = "Aujourd'hui, est le troisieme jour du DIU."
affiche_code_phrase(phrase, 13)    

