Correction de programmes
========================

Support de présentation Sylvain Salvati,
_La correction des programmes : logique, mathématiques et programmation_

* [première séance](s1.pdf) 
* [seconde séance](s2.pdf) 

Fichiers utilisés lors de la présentation

* quelques exemples :
  - sémantique opérationelle :
    - séquentialité : [expr1.py](expr1.py), [expr2.py](expr2.py)
    - évaluation stricte : [factoriel.py](factoriel.py), [fact.hs](fact.hs)
    - effets de bord : [effet.py](effet.py)
  - correction de programmes :
    - exemple d'utilisation de la logique de Hoare : [pgcd.py](pgcd.py)
    - exemple d'utilisation des assertions : [pgcd_assert.py](pgcd_assert.py)
    - exemple d'invariant informel : [iterator.py](iterator.py)



