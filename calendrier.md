Calendrier 
==========

[DIU Enseigner l'informatique au lycée](./Readme.md)

[[_TOC_]]

Janvier 2021
============

jeudi 7 janvier 2021
--------------------

C'est la rentrée ! 

### « amphi » de rentrée ###

Benoit Papegay et Philippe Marquet

[support de présentation](doc/2021-01-diueil-slide.pdf) /
[4 par page ](doc/2021-01-diueil-4up.pdf) /
[(fichier source Markdown)](doc/2021-01-diueil-4up.md)

| quand | qui  | où   | 
| ----  | ---- | ---- | 
| 9h-10h      | groupe 1 | salle A11, bâtiment M5 💬|
|             | groupe 2 | salle A12, bâtiment M5 | 
|             | groupe 3 | salle A13, bâtiment M5 | 
|             | groupe 4 | salle A14, bâtiment M5 | 

> Salles A11 à A14 au 1er étage du bâtiment M5  
> Accès au bâtiment M5 : [carte](https://osm.org/go/0B1fzL6bh--?m=) et [plan du campus](https://www.univ-lille.fr/fileadmin/user_upload/autres/Plan-site-Ulille-contact-cite%CC%81-scientifique.pdf)
  
### bloc 3 — Architectures matérielles et robotique, systèmes et réseaux ###
  
[Prise en main de l'environnement de travail](bloc3/Readme.md#prise-en-main-de-lenvironnement)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 10h15-12h15 | groupe 1 | M5-A11 | Philippe |
|             | groupe 2 | M5-A12 | Thomas |
|             | groupe 3 | M5-A13 | Frédéric |
|             | groupe 4 | M5-A14 | Benoit |

### bloc 1 — Représentation des données et programmation ###

[Premiers travaux pratiques, Python, Thonny](bloc1/Readme.md#premiers-pas-avec-thonny-et-python)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 13h45-17h   | groupe 1 | M5-A11 | Jean-Christophe |
|             | groupe 2 | M5-A12 | Philippe |
|             | groupe 3 | M5-A13 | Benoit |
|             | groupe 4 | M5-A14 | Patricia |

vendredi 8 janvier 2021
-----------------------

### bloc 3 — Architectures matérielles et robotique, systèmes et réseaux ###

conférence [Turing, von Neumann, ... architecture des machines
informatiques et des systèmes d'exploitation](bloc3/Readme.md#architecture-des-machines-informatiques-et-des-syst%C3%A8mes-dexploitation)  
par Gilles Grimaud

| quand | qui  | où   | 
| ----  | ---- | ---- | 
| 9h-10h      | groupe 1 | M5-A11 |
|             | groupe 2 | M5-A12 💬|
|             | groupe 3 | M5-A13 | 
|             | groupe 4 | M5-A14 | 

### bloc 2 — Algorithmique ###

Activité d'informatique sans ordinateur [tris sans ordi](bloc2/Readme.md#premi%C3%A8res-activit%C3%A9s-sur-les-tris)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 10h15-12h15 | groupe 1 | M5-A1 | Laetitia |
|             | groupe 2 | M5-A2 | Francesco |
|             | groupe 3 | M5-A3 | Philippe |
|             | groupe 4 | M5-A6 | Patricia |

_Était prévu une poursuite éventuelle avec un TP [Reconnaître et programmer les tris](bloc2/Readme.md#premi%C3%A8res-activit%C3%A9s-sur-les-tris)_

### bloc 3 — Architectures matérielles et robotique, systèmes et réseaux ###

présentation [Système d'exploitation — système de fichiers,  processus, shell](bloc3/Readme.md#syst%C3%A8me-dexploitation)  
par Philippe Marquet

| quand | qui  | où   | 
| ----  | ---- | ---- | 
| 13h15-14h15 | groupe 1 | M5-A11 |
|             | groupe 2 | M5-A12 |
|             | groupe 3 | M5-A13 💬|
|             | groupe 4 | M5-A14 |

[TP Initiation à UNIX, à l'interpréteur de commandes (suite et fin...)](bloc3/Readme.md#prise-en-main-de-lenvironnement)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 14h30-16h30 | groupe 1 | M5-A11 | Philippe |
|             | groupe 2 | M5-A12 | Thomas |
|             | groupe 3 | M5-A13 | Laurent et Patrice |
|             | groupe 4 | M5-A14 | Benoit |

mardi 12 janvier 2021
---------------------

### bloc 1 — Représentation des données et programmation ###

présentation [Bonnes pratiques de programmation Python](bloc1/Readme.md#bonnes-pratiques-de-programmation-python)  
par Jean-Christophe Routier 

| quand | qui  | où   | 
| ----  | ---- | ---- | 
| 9h-10h      | groupe 1 | M5-A11 |
|             | groupe 2 | M5-A12 |
|             | groupe 3 | M5-A13 |
|             | groupe 4 | M5-A14 💬|

[Second pas de programmation avec Python](bloc1/Readme.md#seconds-pas-avec-python)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 10h15-12h15 | groupe 1 | M5-A11 | Sylvain |
|             | groupe 2 | M5-A12 | Philippe |
|             | groupe 3 | M5-A13 | Francesco |
|             | groupe 4 | M5-A14 | Laetitia |

### bloc 2 — Algorithmique ###

* TP [Reconnaître et programmer les tris](bloc2/Readme.md#premi%C3%A8res-activit%C3%A9s-sur-les-tris)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 13h15-16h30 | groupe 1 | M5-A11 | Laetitia |
|             | groupe 2 | M5-A12 | Francesco |
|             | groupe 3 | M5-A13 | Philippe |
|             | groupe 4 | M5-A14 | Jean-Stéphane |

mercredi 13 janvier 2021
------------------------

### bloc 1 — Représentation des données et programmation ###

présentation [Récursivité](bloc1/Readme.md#r%C3%A9cursivit%C3%A9)  
par Jean-Christophe

| quand | qui  | où   | 
| ----  | ---- | ---- | 
| 9h-10h      | groupe 1 | M5-A11 💬|
|             | groupe 2 | M5-A12 |
|             | groupe 3 | M5-A13 |
|             | groupe 4 | M5-A14 |

Travaux dirigés et activité sans ordinateur
 [Récursivité](bloc1/Readme.md#r%C3%A9cursivit%C3%A9)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 10h15-12h15 | groupe 1 | M5-A1 | Maude |
|             | groupe 2 | M5-A2 | Frédéric |
|             | groupe 3 | M5-A3 | Pierre |
|             | groupe 4 | M5-A6 | Philippe |

### bloc 2 — Algorithmique ###

TP [Analyse en temps d'exécution des tris](bloc2/Readme.md#analyse-des-algorithmes-de-tris)

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 13h15-15h15 | groupe 1 | M5-A11 | Benoit |
|             | groupe 2 | M5-A12 | Francesco |
|             | groupe 3 | M5-A13 | Philippe |
|             | groupe 4 | M5-A14 | Jean-Stéphane |

présentation [Analyse théorique des algorithmes, illustration sur les algorithmes de tri](bloc2/Readme.md#analyse-des-algorithmes-de-tris)  
par Benoit

| quand | qui  | où   | 
| ----  | ---- | ---- | 
| 15h30-16h30 | groupe 1 | M5-A11 |
|             | groupe 2 | M5-A12 💬|
|             | groupe 3 | M5-A13 | 
|             | groupe 4 | M5-A14 | 

jeudi 14 janvier 2021
---------------------

### bloc 1 — Représentation des données et programmation ###

Travaux pratiques [Récursivité](bloc1/Readme.md#r%C3%A9cursivit%C3%A9)
<!-- puis [lancement projet]() -->

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 9h-12h15    | groupe 1 | M5-A11 | Maude |
|             | groupe 2 | M5-A12 | Frédéric |
|             | groupe 3 | M5-A13 | Patricia (puis Laurent) |
|             | groupe 4 | M5-A14 | Philippe |

### bloc 2 — Algorithmique ###

Travaux dirigées [Analyse de complexité d'algorithmes](bloc2/Readme.md#analyse-des-algorithmes-de-tris)  
TP [Complexité, utilisation de décorateurs](bloc2/Readme.md#analyse-des-algorithmes-de-tris) 

| quand | qui  | où   | avec qui |
| ----  | ---- | ---- | ----     |
| 13h15-16h30 | groupe 1 | M5-A1 puis M5-A11 | Benoit |
|             | groupe 2 | M5-A2 puis M5-A12 | Patricia |
|             | groupe 3 | M5-A3 puis M5-A13 | Philippe |
|             | groupe 4 | M5-A6 puis M5-A14 | Jean-Stéphane |


Juin-juillet 2021
=================

Travaux préparatoires
---------------------

Bloc 1. Pour le mercredi 30 juin, travail préparatoire de prise
d'informations, d'un tout petit peu de programamtion, de lecture.

* [préparation programmation](bloc1/prepaChicon/readme.md)

mercredi 30 juin 2021
---------------------

### bloc 1 — Représentation des données et programmation

* [Introduction aux modules et dictionnaires, et exceptions](bloc1/Readme.md#modules-dictionnaires-et-exceptions)
* [TP Course du chicon](bloc1/Readme.md#course-du-chicon)

| quand       | qui      | où         | avec qui        |
| ----------- | -------- | ---------- | --------------- |
| 9h-10h15    | tous     | M5-Bacchus | Jean-Christophe |
| 10h15-12h15 | groupe A | M5-A14     | Philippe        |
|             | groupe B | M5-A12     | Jean-Christophe |
|             | groupe C | M5-A13     | Jean-Stéphane   | 

### bloc 2 — Algorithmique

* [Correction de programmes — cours](bloc2/Readme.md#correction-de-programme)
* [Algorithmes gloutons — TD](bloc2/Readme.md#algorithmes-gloutons)

| quand       | qui      | où         | avec qui      |
| ----------- | -------- | ---------- | ------------- |
| 13h15-14h30 | tous     | M5-Bacchus | Sylvain       |
| 14h30-16h30 | groupe A | M5-A1      | Frédéric      |
|             | groupe B | M5-A2      | Francesco     |
|             | groupe C | M5-A3      | Jean-Stéphane |
    
jeudi 1er juillet 2021
----------------------

### bloc 3 — Architectures matérielles et robotique, systèmes et réseaux

* Introduction aux réseaux informatiques — cours
* [Protocoles réseaux — activités débranchées](bloc3/reseau3/debranche.md)

| quand       | qui      | où         | avec qui          |
| ----------- | -------- | ---------- | ----------------- |
| 9h-10h30    | tous     | M5-Bacchus | Thomas et Laurent |
| 10h30-12h15 | groupe A | M5-A1      | Thomas            |
|             | groupe B | M5-A2      | Laurent           |
|             | groupe C | M5-A3      | Xavier            |

### bloc 1 ou bloc 3

* bloc 1 — [TP course du chicon, suite et fin](bloc1/Readme.md#course-du-chicon)
* bloc 3 — [TP de réseau](bloc3/reseau3/configuration.md)

| quand       | qui      | quoi               | où     | avec qui          |
| ----------- | -------- | ------------------ | ------ | ----------------- |
| 13h15-16h30 | groupe A | TP bloc 3          | M5-A13 | Thomas et Laurent |
|             | groupe B | TP bloc 1 — chicon | M5-A12 | Jean-Chistophe    |
|             | groupe C | TP bloc 1 — chicon | M5-A14 | Benoit            |
    
vendredi 2 juillet 2021
-----------------------

### bloc 2 — Algorithmique

* [Correction de programmes — cours (suite et fin)](bloc2/Readme.md#correction-de-programme)
* [Algorithmes gloutons, le voyageur de commerce — TP](bloc2/Readme.md#algorithmes-gloutons) 

| quand       | qui      | où         | avec qui  |
| ----------- | -------- | ---------- | --------- |
| 9h-10h15    | tous     | M5-Bacchus | Sylvain   |
| 10h15-12h15 | groupe A | M5-A14     | Frédéric  |
|             | groupe B | M5-A12     | Francesco |
|             | groupe C | M5-A13     | Laetitia  |

### bloc 1 ou bloc 3

* bloc 1 — [TP course du chicon, suite et fin](bloc1/Readme.md#course-du-chicon)
* bloc 1 — [TP Wator](bloc1/Readme.md#wator)
* bloc 3 — [TP de réseau](bloc3/reseau3/configuration.md)

| quand       | qui      | quoi               | où     | avec qui          |
| ----------- | -------- | ------------------ | ------ | ----------------- |
| 13h15-16h30 | groupe A | TP bloc 1 — chicon | M5-A14 | Philippe          |
|             | groupe B | TP bloc 3          | M5-A13 | Thomas et Laurent |
|             | groupe C | TP bloc 1 — wator  | M5-A12 | Francesco         |
    
lundi 5 juillet 2021
--------------------

### bloc 1 ou bloc 3

* bloc 1 — [TP Wator](bloc1/Readme.md#wator)
* bloc 3 — [TP de réseau](bloc3/reseau3/configuration.md)

| quand    | qui      | quoi              | où     | avec qui           |
| -------- | -------- | ----------------- | ------ | ------------------ |
| 9h-12h15 | groupe A | TP bloc 1 — wator | M5-A14 | Jean-Christophe    |
|          | groupe B | TP bloc 1 — wator | M5-A12 | Philippe, Laetitia |
|          | groupe C | TP bloc 3         | M5-A13 | Xavier et Benoit   |

### bloc 2 — Algorithmique

* [Algorithme des k plus proches voisins — cours puis TD](bloc2/Readme.md#algorithme-des-k-plus-proches-voisins)

| quand       | qui      | où         | avec qui |
| ----------- | -------- | ---------- | -------- |
| 13h15-14h30 | tous     | M5-Bacchus | Laetitia |
| 14h30-16h30 | groupe A | M5-A1      | Frédéric |
|             | groupe B | M5-A2      | Laetitia |
|             | groupe C | M5-A3      | Patricia |
    
mardi 6 juillet 2021
--------------------

### bloc 2 — Algorithmique

* [Algorithme des k plus proches voisins — TP](bloc2/Readme.md#algorithme-des-k-plus-proches-voisins) 

| quand    | qui      | où     | avec qui  |
| -------- | -------- | ------ | --------- |
| 9h-12h15 | groupe A | M5-A14 | Frédéric  |
|          | groupe B | M5-A12 | Francesco |
|          | groupe C | M5-A13 | Patricia  |

### bloc 1 — Représentation des données et programmation

* [bonnes pratiques 2 — cours](bloc1/Readme.md#bonnes-pratiques-de-programmation-2)
* [TP Wator — suite et fin](bloc1/Readme.md#wator)

| quand       | qui      | où         | avec qui        |
| ----------- | -------- | ---------- | --------------- |
| 13h15-14h30 | tous     | M5-Bacchus | Jean-Christophe |
| 14h30-16h30 | groupe A | M5-A14     | Jean-Christophe |
|             | groupe B | M5-A12     | Philippe        |
|             | groupe C | M5-A13     | Francesco       |

<!-- eof --> 
