Calculabilité, décidabilité
===========================

* Calculabilité - Décidabilité, Peut-on tout programmer
  - [support de présentation](tout_programmer_slide.pdf)
  - [document de cours](tout_programmer.pdf) 
  - [source Markdown](tout_programmer.md) 

* _Une preuve pour le lycée de l’indécidabilité du problème de
  l’arrêt_  
  Matthieu JOURNAULT, Pascal LAFOURCADE, Malika MORE et Rémy POULAIN,
  [Colloque DIDAPRO 8 - DIDASTIC](https://www.didapro.org/8/),
  Lille, février 2020 
  - publication [sur le site didapro.org/8](https://www.didapro.org/8/wp-content/uploads/sites/4/2020/01/Didapro_8_paper_17.pdf)
  - matériel activité débranchée [sur le site IREM Clermond-Ferrand](http://www.irem.univ-bpclermont.fr/Indecidabilite-du-probleme-de-l)
	
* _Une machine de Turing... avec des Lego_
  - activité d'informatique débranchée
    [sur le site de Marie Duflot](https://members.loria.fr/MDuflot/files/med/turing.html)
  - [fiche de l'activité](https://members.loria.fr/MDuflot/files/med/doc/Turing/ficheturing.pdf)

* Machines de Turing  
  calepin – support de « TD sur machine » 
  * [machine_turing.ipynb](machine_turing.ipynb)
  * ou en ligne via le serveur `jupyter.fil.univ-lille1.fr`
    accessible à
    [frama.link/diu-ipynb-turing](https://frama.link/diu-ipynb-turing)
  * disponible sous forme d'un
	[fichier PDF](machine_turing.pdf) ou d'un
	[source Python](machine_turing.py)
  * images utilisées dans le calepin
    [tikz_machine_turing.png](tikz_machine_turing.png) /
    [tikz_unite_controle2.png](tikz_unite_controle2.png) /
    [tikz_automate_fini.png](tikz_automate_fini.png)

* Bibliothèque Python `turing_machine.py`
  pour la manipulation de machines de Turing
  - nécessaire au calepin
  - [turing_machine.py](./turing_machine.py)
