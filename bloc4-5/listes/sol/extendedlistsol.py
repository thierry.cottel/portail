from otherlist import *

class Extendedlist(List):

    def length (self):
        """
        Return the length of a list
        
        :return: Number of elements in the list

        >>> l = Extendedlist()
        >>> l.length()
        0
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.length()
        3
        """
        l = 0
        p = self
        while not p.is_empty():
            l += 1
            p = p.tail()
        return l
    
    def length_rec (self):
        """
        Return the length of a list
        
        :return: Number of elements in the list

        >>> l = Extendedlist()
        >>> l.length()
        0
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.length()
        3
        """
        if self.is_empty():
            return 0
        elif self.tail().is_empty():
            return 1
        else:
            return 1 + (self.tail()).length_rec()

    def get(self,i):
        '''
        Get the element at position i (positions start at 0).
        
        :CU: not self.is_empty()

        >>> l = Extendedlist(1, Extendedlist())
        >>> l.get(0)
        1
        >>> l = Extendedlist(4, Extendedlist())
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.get(3)
        4
        >>> l.get(0)
        1
        '''
        p = self
        while not p.is_empty() and i != 0:
            i -= 1;
            p = p.tail()
        if p is None:
            raise Exception("applying get on an empty list")
        return p.head()
    
    def get_rec(self,i):
        '''
        Get the element at position i (positions start at 0).
        
        :CU: not self.is_empty()

        >>> l = Extendedlist(1, Extendedlist())
        >>> l.get(0)
        1
        >>> l = Extendedlist(4, Extendedlist())
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.get(3)
        4
        >>> l.get(0)
        1
        '''
        try:
            if i == 0:
                return self.head()
            else:
                return self.tail().get(i-1)
        except:
            raise Exception("applying get on an empty list")
            

    def search (self, e):
        """
        Return whether e exists in the list

        :return: True iff e is an element of the list
        
        >>> l = Extendedlist()
        >>> l.search(0)
        False
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.search(1)
        True
        >>> l.search(3)
        True
        >>> l.search(4)
        False
        """
        try:
            if e == self.head():
                return True
            else:
                return self.tail().search(e)
        except ListError:
            return False
        
    def toString (self):
        """
        Return a string representation of the list

        >>> l = Extendedlist()
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.toString()
        '1 2 3'
        """
        if self.is_empty():
            return ""
        elif self.tail().is_empty():
            return str(self.head())
        else:
            return str(self.head()) + " " + self.tail().toString()
        
    def toPythonList (self):
        """
        Return the Python list corresponding to the list

        :return: A Python list whose length and elements are identical to `self`.

        >>> l = Extendedlist()
        >>> l.toPythonList()
        []
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.toPythonList()
        [1, 2, 3]
        """
        l = []
        p = self
        while not p.is_empty():
            l.append(p.head())
            p = p.tail()
        return l

    def sortedInsert(self, x):
        '''
        >>> l = Extendedlist()
        >>> l = Extendedlist(4,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.sortedInsert(3)
        (1.(2.(3.(4.()))))
        >>> l = Extendedlist(4, Extendedlist())
        >>> l.sortedInsert(10)
        (4.(10.()))
        >>> l = Extendedlist(4, Extendedlist())
        >>> l.sortedInsert(1)
        (1.(4.()))
        '''
        if self.is_empty():
            return Extendedlist(x, self)
        if x < self.head():
            return Extendedlist(x, self)
        return Extendedlist(self.head(), self.tail().sortedInsert(x))

    def reverse(self):
        '''
        :return: A fresh list containing the same elements as `self`but in reversed order.

        >>> l = Extendedlist()
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l
        (1.(2.(3.())))
        >>> r = l.reverse()
        >>> r
        (3.(2.(1.())))
        '''                
        return self.__reverse_rec(Extendedlist())

    def __reverse_rec(self, accumulated_list):
        if self.is_empty():
            return accumulated_list
        accumulated_list = Extendedlist(self.head(), accumulated_list)
        return self.tail().__reverse_rec(accumulated_list)


    def __str__ (self):
        return self.toString()
    
    def __getitem__ (self,i):
        return self.get(i)

    def __len__ (self):
        return self.length()

    def __next__ (self):
        try:            
            v = self.__iter.head()
            self.__iter = self.__iter.tail()
            return v
        except:
            raise StopIteration
        
    def __iter__ (self):
        """
        Implantation très sommaire d'un itérateur. Ne permet pas d'itérer
        sur la même liste dans une boucle imbriquee

        """
        self.__iter = self
        return self


    def append (self, e):
        '''
        Append element e at the end of the list.
        Raises Listerror if the list is empty.

        >>> l = Extendedlist()
        >>> l.append(1)
        Traceback (most recent call last):
        ...
        otherlist.ListError        
        >>> l = Extendedlist(1,Extendedlist())        
        >>> l.append(2)
        >>> l
        (1.(2.()))
        >>> l.append(3)
        >>> l
        (1.(2.(3.())))
        '''
        if self.is_empty():
            raise ListError("")
        elif self.tail().is_empty():
            self.set_tail(Extendedlist(e,List()))
        else:
            self.tail().append(e)
            
if __name__ == "__main__":
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=True)
    
